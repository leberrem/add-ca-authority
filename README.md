Role Name
=========

Add CA trusted authoritiy.

Requirements
------------

No requirements.

Role Variables
--------------

```yaml
ca_certificates_trust_file: files/CA.crt
```

Dependencies
------------

No dependencies.

Example Playbook
----------------

Including an example of how to use your role :

```yaml
- hosts: servers
  become : yes
  tasks:
    - name: add-ca-authority
      include_role:
        name: add-ca-authority
      var:
        ca_certificates_trust_file: files/CA.crt
```

License
-------

Apache v2.0

Author Information
------------------

Mikaël LE BERRE
